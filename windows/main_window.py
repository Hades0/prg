from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5.QtWidgets import QFileDialog
from ui_py import main_window
import json

class MainWindow(QMainWindow, main_window.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.number = 0
        self.Res = 0
        self.Pos = 0
        self.Neg = 0
        self.Skip = 0
        self.testDow.clicked.connect(self.btn_dow_json)
        self.testStart.clicked.connect(self.btn_start_json)
        self.answer_btn.clicked.connect(self.btn_answer)
        self.tesFinal.clicked.connect(self.btn_final)
        self.answer_btn.setEnabled(False)
        self.tesFinal.setEnabled(False)
    def btn_dow_json(self):
        fd = QFileDialog.getOpenFileName(self,
                                         f'{self.windowTitle()} [путь для открытия]',
                                         '',
                                         'JSON (*.json)')
        with open(fd[0], 'r', encoding='utf-8', newline='') as f:
            self.data = json.load(f)
            self.allAnswers.setText(f'Всего загружено вопросов: {len(self.data)}')

    def btn_start_json(self):
        self.answer_lebel.setText(self.data[self.number]["question"])
        self.var1.setText(self.data[self.number]["ans_1"])
        self.var2.setText(self.data[self.number]["ans_2"])
        self.var3.setText(self.data[self.number]["ans_3"])
        self.var4.setText(self.data[self.number]["ans_4"])
        self.testDow.setEnabled(False)
        self.testStart.setEnabled(False)
        self.answer_btn.setEnabled(True)
        self.tesFinal.setEnabled(True)

    def btn_answer(self):
        ans = self.data[self.number]["correct_answer"]
        if self.var1.isChecked():
            if ans == 'ans_1':
                self.Pos += 1
                self.posAnswers.setText(f'Правильных ответов: {self.Pos}')
            else:
                self.Neg += 1
                self.negAnswers.setText(f'Неправильных ответов: {self.Neg}')
        elif self.var2.isChecked():
            if ans == 'ans_2':
                self.Pos += 1
                self.posAnswers.setText(f'Правильных ответов: {self.Pos}')
            else:
                self.Neg += 1
                self.negAnswers.setText(f'Неправильных ответов: {self.Neg}')
        elif self.var3.isChecked():
            if ans == 'ans_3':
                self.Pos += 1
                self.posAnswers.setText(f'Правильных ответов: {self.Pos}')
            else:
                self.Neg += 1
                self.negAnswers.setText(f'Неправильных ответов: {self.Neg}')
        elif self.var4.isChecked():
            if ans == 'ans_4':
                self.Pos += 1
                self.posAnswers.setText(f'Правильных ответов: {self.Pos}')
            else:
                self.Neg += 1
                self.negAnswers.setText(f'Неправильных ответов: {self.Neg}')
        else:
            self.Skip += 1
            self.skipAnswers.setText(f'Не отвеченных: {self.Skip}')
        self.Res += 1
        self.allResult.setText(f'Всего вопросов: {self.Res}')
        if len(self.data) - 1 != self.number:
            self.number += 1
            self.answer_lebel.setText(self.data[self.number]["question"])
            self.var1.setText(self.data[self.number]["ans_1"])
            self.var2.setText(self.data[self.number]["ans_2"])
            self.var3.setText(self.data[self.number]["ans_3"])
            self.var4.setText(self.data[self.number]["ans_4"])
        else:
            self.btn_final()
    def btn_final(self):
        QMessageBox.warning(self,
                            self.windowTitle(),
                            'Вы завершили тест')
        self.testDow.setEnabled(True)
        self.testStart.setEnabled(True)
        self.answer_btn.setEnabled(False)
        self.tesFinal.setEnabled(False)
        self.number = 0
        self.Res = 0
        self.Pos = 0
        self.Neg = 0
        self.Skip = 0
        self.Skip = self.Skip + len(self.data) - self.number
        self.Res = self.Res + len(self.data) - self.number
        self.skipAnswers.setText(f'Не отвеченных: {self.Skip}')
        self.allResult.setText(f'Всего вопросов: {self.Res}')






